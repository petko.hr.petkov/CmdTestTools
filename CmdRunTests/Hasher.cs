﻿/**
 * Data (and file) hashing functonality wrapper.
 * 
 * Copyright (C) 2017, Volley Labs Ltd.
 * Author: Ivan (Jonan) Georgiev
 */
using System;
using System.Security.Cryptography;
using System.IO;
using System.Diagnostics;

namespace CmdTestTools
{
    class Hasher
    {
        /// <summary>
        /// The actual stream to be hashed.
        /// </summary>
        private Stream _inputStream;

        /// <summary>
        /// The actual hash, or null if not calculated yet.
        /// </summary>
        private byte[] _hash;

        /// <summary>
        /// Makes a Hahser instance with the given input stream, which will directly
        /// be used by the hashing algorithm.
        /// </summary>
        /// <param name="stream">The input stream to be hashed.</param>
        public Hasher(Stream stream)
        {
            _inputStream = stream;
            _hash = null;
        }

        /// <summary>
        /// Makes the actual SHA1 calculation of the hash.
        /// </summary>
        /// <returns>The byte array of the hash (20 bytes).</returns>
        public byte[] ComputeHash()
        {
            Debug.Assert(_inputStream != null);

            using (SHA1 sha = new SHA1CryptoServiceProvider())
            {
                _hash = sha.ComputeHash(_inputStream);
                Debug.Assert(_hash != null);
            }
            return _hash;
        }


        /// <summary>
        /// Returns the calculated hash, or forces a calculation if such hasnt' happened yet,
        /// </summary>
        public byte[] Hash
        {
            get
            {
                if (_hash == null)
                    ComputeHash();
                return _hash;
            }
        }

        /// <summary>
        /// Returns a Base64 encoding of the hash, forcing calculation if necessary.
        /// </summary>
        public string StringHash
        {
            get
            {
                return Convert.ToBase64String(this.Hash);
            }
        }
    }
}
