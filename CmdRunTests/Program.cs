﻿/**
 * The CmdRunTests execution code.
 * 
 * Copyright (C) 2017, Volley Labs Ltd.
 * Author: Ivan (Jonan) Georgiev
 */
using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using CmdRunTests;

namespace CmdTestTools
{
    internal class ParserDefinition
    {
        public Regex pattern
        { get; set; }

        public string subst
        { get; set; }

        public bool multilined
        { get; set; }

        public ParserDefinition(Regex regex, string substitution, bool multi)
        {
            this.pattern = regex;
            this.subst = substitution;
            this.multilined = multi;
        }
    }

    internal class RunnerInfo
    {
        public TextWriter errorWriter
        { get; }

        public TextWriter outputWriter
        { get; }

        public List<ParserDefinition> errorParsers
        { get; }

        public List<ParserDefinition> outputParsers
        { get; }

        public TaskDescription task
        { get; }

        public RunnerInfo(TaskDescription task, TextWriter errLog, TextWriter outLog, List<ParserDefinition> errFilter, List<ParserDefinition> outFilter)
        {
            this.errorWriter = errLog;
            this.outputWriter = outLog;
            this.errorParsers = errFilter;
            this.outputParsers = outFilter;
            this.task = task;
        }
    }

    public class CmdTesterProgram
    {
        static private void ShowHelp()
        {
            Console.Error.WriteLine("Lightweight command line testing tool.\nCopyright (C) 2017, VolleyLabs Ltd.\n");
            Console.Error.WriteLine("Usage: CmdTester [<options>] <tests-description-json>");
            Console.Error.WriteLine("       CmdTester /h");
            Console.Error.WriteLine("\nOptions:");
            Console.Error.WriteLine("  /c[onfiguration] <config-json>    Specify the parsers and formatters configuration JSON file.");
            Console.Error.WriteLine("  /t[imeout] <seconds>              The default timeout for each of the tasks, in seconds. Default is 10.");
            Console.Error.WriteLine("  /k[ey]                            Provide key:value pair for substituion in tests description file.");
            Console.Error.WriteLine("                                    Can have multiple such entries.");
            Console.Error.WriteLine("  /w[ait]                           Make the tasks wait for each other, i.e. - run synchronously.");
            Console.Error.WriteLine("  /p[reserve]                       Preserve the outputs, i.e. don't run the `cleanup` step.");
            Console.Error.WriteLine("  /? | /h[elp]                      Output this help information.");
            Console.Error.WriteLine("  <tests-description-json>          The mandatory tests description JSON file.");
            Console.Error.WriteLine();
        }

        private static int ShellTask(string command)
        {
            if (command != null)
            {
                Runner runner = new Runner("cmd", "/c " + command, RunnerMode.Synchronous);
                runner.Start();
                return runner.ExitCode;
            }
            else
                return 0;
        }

        private static void ParseKeyValue(string pair, Dictionary<string, string> dict)
        {
            string[] couple = pair.Trim().Split(new char[] { ':' });
            if (couple.Length != 2)
                throw new FormatException("Invalid key-value pair: `" + pair + "` !");
            dict.Add("{{" + couple[0] + "}}", couple[1]);
        }

        private static Dictionary<string, string> BuildTaskDictionary(TaskDescription task, string explanation)
        {
            return new Dictionary<string, string>
            {
                { "{{task.name}}", task.name },
                { "{{task.exe}}", task.exe },
                { "{{task.arguments}}", task.arguments },
                { "{{task.report}}", task.report },
                { "{{explanation}}", explanation }
            };
        }

        private const double DEF_TIMEOUT = 60.0;

        static int Main(string[] args)
        {
            try
            {
                string testset = null;
                string configuration = null;
                double timeout = DEF_TIMEOUT;
                bool preserve = false;
                bool wait = false;
                Dictionary<string, string> keyValues = new Dictionary<string, string>();

                for (int i = 0; i < args.Length; ++i)
                {
                    var arg = args[i];
                    if (arg.StartsWith("/c")) // config argument
                        configuration = args[++i];
                    else if (arg.StartsWith("/w")) // synchronous flag argument
                        wait = true;
                    else if (arg.StartsWith("/p")) // preserve the outputs
                        preserve = true;
                    else if (arg.StartsWith("/h") || arg.Equals("/?")) // asking for help
                        throw new Exception("");
                    else if (arg.StartsWith("/t")) // the default timeout
                        timeout = Double.Parse(args[++i]);
                    else if (arg.StartsWith("/k")) // key-value pair specification
                        ParseKeyValue(args[++i], keyValues);
                    else
                        testset = arg;
                }

                if (testset == null)
                    throw new InvalidProgramException("You cannot skip specifying <tests-description-json> argument!");

                CmdTesterProgram prg;
                using (FileStream taskFile = File.Open(testset, FileMode.Open, FileAccess.Read))
                using (FileStream configFile = File.Open(configuration, FileMode.Open, FileAccess.Read))
                {
                    prg = new CmdTesterProgram(
                        keyValues,
                        taskFile,
                        configFile,
                        timeout,
                        wait,
                        preserve);
                }

                Console.WriteLine("Finished!");
                return prg.Result;
            }
            catch (Exception e)
            {
                ShowHelp();
                if (e.Message.Length > 0)
                {
                    Console.Error.WriteLine("Err: {0}", e.Message);
#if DEBUG
                    for (; e != null; e = e.InnerException)
                    {
                        Console.Error.WriteLine("----------");
                        Console.Error.WriteLine(e.Message);
                        Console.Error.WriteLine(e.StackTrace);
                    }
#endif
                }
                else
                    Console.Error.WriteLine("Enjoy!");
                return -1;
            }
        }

        #region Instance execution

        private Dictionary<string, ParserDefinition> _parsers = new Dictionary<string, ParserDefinition>();
        private Dictionary<string, string[]> _formatters = new Dictionary<string, string[]>();
        private Dictionary<string, string> _keys;

        private const string FORMATDEF_NAME = @"default";
        private const string FORMATDEF_OK = "<<< [{{task.name}}]: [Success]";
        private const string FORMATDEF_FAILURE = "<<< [{{task.name}}]: [Failure]\n{{explanation}}";
        private const string PARSECLEAN_NAME = @"cleaner";
        private const string PARSECLEAN_REGEX = @"/.*//";
        private const string DEF_PREPARE_EXPLAIN = "Preparation command error!";

        private const int RESULT_OK = 0;
        private const int RESULT_ERR = 1;

        public CmdTesterProgram(
            Dictionary<string, string> keyValues,
            Stream taskStream, 
            Stream configStream, 
            double timeout,
            bool wait,
            bool preserve)
        {
            _keys = keyValues;
            // Add the default formatters and parsers now, so it can be overriden, if desired afterwards.
            _formatters.Add(FORMATDEF_NAME, new string[] { FORMATDEF_OK, FORMATDEF_FAILURE });
            _parsers.Add(PARSECLEAN_NAME, ParseRegexDefinition(PARSECLEAN_REGEX, false));

            if (configStream != null)
                LoadConfiguration(configStream);

            // Go for task deserialization and process it.
            var serializer = new DataContractJsonSerializer(typeof(TaskDescription[]));
            TaskDescription[] _tasks = (TaskDescription[])serializer.ReadObject(taskStream);

            // Some parallelism housekeeping stuff - the reproting synchronizer and the signal-finish event
            object reportGate = new object();

            ManualResetEvent finishedEvent = new ManualResetEvent(false);
            int finishedCount = 0;

            // Define our runner finish delegate.
            Runner.FinishedEvent onFinish = delegate (Runner runner, bool killed)
            {
                string explanation;
                int resultIndex;

                RunnerInfo info = (RunnerInfo)runner.UserInfo;

                if (!killed)
                {
                    // Parse the task output and error streams and check whether expectations are met.
                    resultIndex = CheckExpectations(info.task, runner.ExitCode, info.outputWriter.ToString(), info.errorWriter.ToString(), out explanation);
                }
                else
                {
                    explanation = "Timed out!";
                    resultIndex = RESULT_ERR;
                }

                info.errorWriter.Close();
                info.outputWriter.Close();

                // Do the cleaning, if such is provided.
                if (!preserve)
                    ShellTask(GetEnrichedValue(info.task.cleanup));

                lock (reportGate)
                {
                    Console.Out.WriteLine(GetEnrichedValue(
                        ResultFormatter(info.task.report, resultIndex),
                        BuildTaskDictionary(info.task, explanation)));
                    Console.Out.Flush();

                    // Update the result code ensuring the non-zeroes will propagate to the end result.
                    if (resultIndex > this.Result)
                        this.Result = resultIndex;

                    ++finishedCount;
                    if (finishedCount == _tasks.Length) // i.e. this is the last one!
                        finishedEvent.Set();
                }
            };

            Runner.OutputEvent onText = delegate (Runner runner, string line, bool isError)
            {
                Console.Error.WriteLine(line);
                Console.Error.Flush();

                RunnerInfo info = (RunnerInfo)runner.UserInfo;
                ParseSingleLine(isError ? info.errorParsers : info.outputParsers, isError ? info.errorWriter : info.outputWriter, line);
            };

            // Make the actual iteration and execution over tasks
            foreach (TaskDescription task in _tasks)
            {
                // Make the preparation
                int resCode = ShellTask(GetEnrichedValue(task.prepare));

                if (task.exe == null)
                {
                    Console.Out.WriteLine(GetEnrichedValue(ResultFormatter(task.report, resCode), BuildTaskDictionary(task, DEF_PREPARE_EXPLAIN)));
                    Console.Out.Flush();
                    ++finishedCount;
                    continue;
                }

                // The thick definition or variable _per task_.
                string theExe = GetEnrichedValue(task.exe);
                string theArguments = GetEnrichedValue(task.arguments);

                // Mark the beginning of the execution
                Console.Out.WriteLine(">>> [{0}]: {1} {2}", task.name, theExe, theArguments);
                Console.Out.Flush();

                // Create and setup a task runner
                Runner runner = new Runner(
                    theExe,  
                    theArguments, 
                    task.wait || wait ? RunnerMode.Synchronous : RunnerMode.Asynchronous
                    );

                runner.UserInfo = new RunnerInfo(
                    task,
                    PrepareWriter(GetEnrichedValue(task.stderr_file)),
                    PrepareWriter(GetEnrichedValue(task.stdout_file)),
                    PrepareParsers(task.stderr_parsers),
                    PrepareParsers(task.stdout_parsers));

                // convert floating seconds to 100 nano-seconds ticks.
                runner.Timeout = new TimeSpan((long)((task.timeout > 0 ? task.timeout : timeout) * 1000.0 * 1000.0 * 10.0));

                // Make the two handlers work in cooperation...
                runner.OnTextStream = onText;
                runner.OnFinished = onFinish;

                runner.SetOutputRedirect(Encoding.UTF8);

                // Setup the input, if provided.
                if (task.stdin_string != null)
                    runner.SetInput(GetEnrichedValue(task.stdin_file));
                else if (task.stdin_file != null)
                    runner.SetInput(File.ReadAllText(GetEnrichedValue(task.stdin_file)));

                // Let it fly!
                runner.Start();
            }

            finishedEvent.WaitOne();
        }

        public int Result
        {
            get;
            set;
        }

        private ParserDefinition ParseRegexDefinition(string regexString, bool multi)
        {
            char delimiter = regexString[0];
            string[] parts = regexString.Split(delimiter);
            if (parts.Length != 4)
                return null;
            var regex = new Regex(parts[1], RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return new ParserDefinition(regex, parts[2], multi);
        }

        private void LoadConfiguration(Stream configFile)
        {
            // Load and parse the configuration file.
            var serializer = new DataContractJsonSerializer(typeof(ConfigDescription));

            ConfigDescription configuration = (ConfigDescription)serializer.ReadObject(configFile);

            // Iterate and convert regex definition in to Regex object, if such are provided
            if (configuration.parsers != null)
            {
                foreach (ParserDescription entry in configuration.parsers)
                {
                    var regexDef = ParseRegexDefinition(entry.regex, entry.multilined);
                    if (regexDef == null)
                        throw new ArgumentException("Invalid regex provided: " + entry.regex, "regex");
                    else
                        _parsers.Add(entry.name, regexDef);
                }
            }

            // Iterate on formatters and put them in the dictionary, if such are provided
            if (configuration.formatters != null)
                foreach (FormatterDescription entry in configuration.formatters)
                    _formatters.Add(entry.name, new string[] { entry.OK, entry.Failure });

        }

        private int CheckExpectations(TaskDescription task, int exitCode, string taskOutput, string taskError, out string explanation)
        {
            StringBuilder explainBuilder = new StringBuilder();
            int resultIndex = RESULT_OK;
            string expectedText = null;

            foreach (TaskExpectation should in task.expectations)
            {
                // Check for exit code matching
                if (should.exit_code is int && (int)should.exit_code != exitCode)
                {
                    explainBuilder.AppendFormat(" - exit codes doesn't match: Expecting `{0}`, but Got `{1}`!\n", should.exit_code, exitCode);
                    resultIndex = RESULT_ERR;
                }

                // Check for output stream matching
                if (should.output != null && !(expectedText = GetEnrichedValue(should.output)).Equals(taskOutput))
                {
                    explainBuilder.AppendFormat(" - outputs doesn't match: Expecting `{0}`, but Got `{1}`!\n", expectedText, taskOutput);
                    resultIndex = RESULT_ERR;
                }

                // Check for error stream matching
                if (should.error != null && !(expectedText = GetEnrichedValue(should.error)).Equals(taskError))
                {
                    explainBuilder.AppendFormat(" - stderrs doesn't match: Expecting `{0}`, but Got `{1}`!\n", expectedText, taskError);
                    resultIndex = RESULT_ERR;
                }

                // Check for provided file matching
                if (should.file != null)
                {
                    if (should.SHA1 == null && should.size == null)
                        throw new ArgumentNullException("file", "[" + task.name + "] missing both `SHA1` and `size` when a file is specified: '" + should.file + "'!");
                    try
                    {
                        if (should.SHA1 != null)
                        {
                            using (FileStream fstream = File.Open(should.file, FileMode.Open, FileAccess.Read))
                            {
                                Hasher hasher = new Hasher(fstream);
                                if (!should.SHA1.Equals(hasher.StringHash))
                                {
                                    explainBuilder.AppendFormat(" - hash of file '{2}' don't match: Expecting `{0}`, but Got `{1}`!\n", should.SHA1, hasher.StringHash, should.file);
                                    resultIndex = RESULT_ERR;
                                }
                            }
                        }
                        if (should.size != null)
                        {
                            double fileLen = new FileInfo(should.file).Length;
                            Regex rex = new Regex(@"\s*([0-9.]+)\s*([PTGMK][B])?\s*", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                            Match parsed = rex.Match(should.size);
                            if (parsed.Length == 0)
                                throw new ArgumentException("Unrecognized file size notation: " + should.size, "size");

                            double sz = Double.Parse(parsed.Groups[1].ToString());
                            string units = parsed.Groups[2].ToString().ToUpper();
                            if (units.Equals("PB"))
                                fileLen /= 1024.0 * 1024 * 1024 * 1024 * 1024;
                            else if (units.Equals("TB"))
                                fileLen /= 1024.0 * 1024 * 1024 * 1024;
                            else if (units.Equals("GB"))
                                fileLen /= 1024.0 * 1024 * 1024;
                            else if (units.Equals("MB"))
                                fileLen /= 1024.0 * 1024;
                            else if (units.Equals("KB"))
                                fileLen /= 1024.0;

                            if ((int)fileLen != (int)sz)
                            {
                                explainBuilder.AppendFormat(" - size of file '{3}' don't match: Expecting `{0}`, but Got `{1} {2}`!\n", should.size, (int)fileLen, units, should.file);
                                resultIndex = RESULT_ERR;
                            }
                        }
                    }
                    catch (IOException fileEx)
                    {
                        explainBuilder.AppendFormat(" - error opening file '{1}': {0}!\n", fileEx.Message, should.file);
                        resultIndex = RESULT_ERR;
                    }
                    catch (FormatException numEx)
                    {
                        throw new FormatException("Provided size cannot be understood: " + should.size, numEx);
                    }
                }
            }

            explanation = explainBuilder.ToString();
            return resultIndex;
        }

        private string GetEnrichedValue(string value, Dictionary<string, string> keys = null)
        {
            if (value == null)
                return value;
            else if (keys == null)
                keys = _keys;

            if (keys != null)
                foreach (KeyValuePair<string, string> pair in keys)
                    value = value.Replace(pair.Key, pair.Value);
            return value;
        }

        private string ResultFormatter(string name, int index)
        {
            string[] format;
            if (name == null)
                name = FORMATDEF_NAME;
            if (!_formatters.TryGetValue(name, out format))
                return name;
            else
                return format[Math.Abs(index)];
        }

        private static TextWriter PrepareWriter(string outputFile)
        {
            TextWriter writer;
            if (outputFile != null)
                writer = File.CreateText(outputFile);
            else
                writer = new StringWriter();
            return writer;
        }

        private static void ParseSingleLine(List<ParserDefinition> parsers, TextWriter writer, string line)
        {
            if (parsers == null)
                writer.WriteLine(line);
            else
            {
                foreach (var parser in parsers)
                {
                    if (parser.pattern.IsMatch(line))
                    {
                        line = parser.pattern.Replace(line, parser.subst);
                        if (parser.multilined)
                            writer.WriteLine(line);
                        else
                            writer.Write(line);
                        break;
                    }
                }
            }

            writer.Flush();
        }

        private List<ParserDefinition> PrepareParsers(string[] parsersNames)
        {
            if (parsersNames == null)
                return null;
            List<ParserDefinition> parsers = new List<ParserDefinition>(parsersNames.Length);
            foreach (string oneName in parsersNames)
            {
                ParserDefinition oneParser;
                string finalName = GetEnrichedValue(oneName);
                if (!_parsers.TryGetValue(finalName, out oneParser))
                {
                    oneParser = ParseRegexDefinition(finalName, false);
                    if (oneParser == null)
                        throw new ArgumentException("Invalid parser referred: " + finalName, "parsers");
                }
                parsers.Add(oneParser);
            }

            return parsers;
        }

        #endregion
    }
}
